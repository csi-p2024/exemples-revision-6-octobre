<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            $nombre = 3.14;
            $autre = "3.14";
            $reponse = "Rien...";
        ?>
        <h1>Les conditions</h1>
        <h2>Condition 1</h2>
        <?php
            if($nombre >= 0 && $nombre <= 3)
            {
                $reponse = "Vrai";
            }
            else
            {
                $reponse = "Faux";
            }
        ?>
        <p>Réponse: <?= $reponse ?></p>

        <h2>Condition 2</h2>
        <?php
            if($nombre >= 0 & $nombre <= 3)
            {
                echo "<p>Vrai<p>";
            }
            else
            {
        ?>
        <p>Réponse: Faux</p>
        <?php
            }
        ?>

        <h2>Condition 3</h2>
        <?php if($nombre >= 0) : ?>
            <p>Réponse: Vrai</p>
        <?php else : ?>
            <p>Réponse: Faux</p>
        <?php endif; ?>
        
        <h2>Condition 4</h2>
        <?php if($nombre == $autre) : ?>
            <p>Réponse: Vrai</p>
        <?php else : ?>
            <p>Réponse: Faux</p>
        <?php endif; ?>

        <h2>Condition 5</h2>
        <?php if($nombre === $autre) : ?>
            <p>Réponse: Vrai</p>
        <?php else : ?>
            <p>Réponse: Faux</p>
        <?php endif; ?>
            
        <h2>Condition 6</h2>
        <?php 
            $nb = 0;
            if($nb !== false) : ?>
            <p>Réponse: Vrai</p>
        <?php else : ?>
            <p>Réponse: Faux</p>
        <?php endif; ?>
            
            
        <h1>Les itérations (boucles...)</h1>        
        <h2>Itération 1</h2>
        <?php
            while($nb < 5)
            {
                echo "<p>nb= $nb<p>";
                ++$nb;   //$nb=$nb+1
            }
        ?>
        <h2>Itération 2</h2>
        <?php 
            $nb = 0;
            while($nb < 5) : ?>
                <p>nb= <?= ++$nb; ?><p>
        <?php endwhile; ?>
    </body>
</html>
