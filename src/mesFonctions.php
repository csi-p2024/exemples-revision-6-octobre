<?php

function estPair(int $nb) : bool
{
    if($nb%2 === 0)
        return true;
    else
        return false;
}

/**
 * Passage de paramètre par copie de valeur
 * @param int $nb
 */
function modif_1(int $nb)
{
    $nb += 100;
}

/**
 * Passage de paramètre par référence
 * Modifie la variable d'origine
 * @param int $nb
 */
function modif_2(int& $nb)
{
    $nb += 100;
}

/**
 * Même chose avec les tableau...
 * @param array $t
 */
function avecTab(array& $t)
{
    foreach ($t as $k => $v)
    {
        $t[$k] = 2*$v; 
        echo "t[$k] = $t[$k] </br>";
    }
}