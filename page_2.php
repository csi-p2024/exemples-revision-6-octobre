<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ma page 2</title>
    </head>
    <body>
        <h2>Première ligne en php:</h2>
        <p><?php echo "Texte en PHP"; ?></p>
        <h2>Deuxième ligne en php:</h2>
        <?php
            $texte = 'Ceci est une string.';
            $nombre = 3.14;            
        ?>
        <p>La variable $texte: <?php echo $texte ?></p>
        <p>La variable $nombre: <?= $nombre ?></p>
        <?php
            $texte = 3.14;
            $nombre = 'Ceci est une "string".';            
        ?>
        <p>La variable $texte: <?php echo $texte ?></p>
        <p>La variable $nombre: <?= $nombre ?></p>    
        
        <h2>Troisième ligne en php:</h2>
        <?php
            $message_1 = "$texte et $nombre";
            $message_2 = '$texte et $nombre '.$texte. ' et '.$nombre;
        ?>
        <p>La variable $message_1: <?php echo $message_1 ?></p>
        <p>La variable $message_2: <?= $message_2 ?></p>
            
    </body>
</html>
