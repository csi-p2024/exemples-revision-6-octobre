<?php 
    declare(strict_types=1);

    require_once "src/mesFonctions.php";
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Les fonctions</title>
    </head>
    <body>
        <p>Voir le fichier /src/mesFonctions.php...</p>
        <h2>Pair</h2>
        <?php
        if (estPair(4))
            echo "Pair";
        else
            echo "Impair";
        ?>
        <h2>Modif_1</h2>
        <?php
            $unNombre = 5;
            modif_1($unNombre);
            echo $unNombre;
            echo "</br>";
            modif_2($unNombre);
            echo $unNombre;
        ?>
        <h2>avecTab</h2>
        <?php
            $tab = [9, 10, 20, 30];
            var_dump($tab);
            avecTab($tab);
            var_dump($tab);
        ?>
        
    </body>
</html>
