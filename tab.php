<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Les tableaux</title>
    </head>
    <body>
        <?php
        $tab1 = [1, 2];
        $tab1[] = 3;
        $tab1[] = "Du texte";
        $tab1[10] = 3.14;
        $tab1[7] = 6.28;
        $tab1['case1'] = "Texte dans case 1";
        $tab1[] = "Quelle case?";
        $tab1[8] = [6.28, 12.56];
        
        var_dump($tab1);
        ?>
        <p>$tab1: <?= $tab1[0]?></p>
        <p>$tab1: <?= $tab1[5]?></p>
        <p>$tab1: <?= $tab1['case1']?></p>
        
        <h2>Tout le tableau</h2>
        <?php foreach($tab1 as $key => $val) : ?>
            <?php if(is_array($val)) :?>
                <?php foreach($val as $k => $v) : ?>
                <p>&nbsp;&nbsp;$tab1[<?= $key; ?>][<?= $k; ?>]: <?= $v; ?></p>                    
                <?php endforeach; ?>
            <?php else: ?>
            <p>$tab1[<?= $key; ?>]: <?= $val; ?></p>
            <?php endif; ?>
        <?php endforeach; ?>
            
            <h2>Tableau 2</h2>
            <?php
                $tab2 = [
                    'nom' => 'Personne 1',
                    'age' => 30
                ];
                $tab3 = [$tab2];
                $tab3[] =  [
                    'nom' => 'Personne 2',
                    'age' => 40
                ];
                $tab3[5] =  [
                    'nom' => 'Personne 3',
                    'age' => 50
                ];
                var_dump($tab3);
            ?>
            <table>
                <tr><th>Id</th><th>Nom</th><th>Age</th></tr>
        <?php foreach($tab3 as $key => $val) : ?>
                <tr>
                    <td><?= $key; ?></td>
                    <td><?= $val['nom']; ?></td>
                    <td><?= $val['age']; ?></td>
                </tr>
        <?php endforeach; ?>
            </table>
    </body>
</html>
